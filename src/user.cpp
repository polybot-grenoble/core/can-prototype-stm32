#include "core/core.hpp"

void Talos_initialisation() {
    /**
     * Code to be run on startup and when exiting error state.
     * Make sure you don't init your pins twice.
     */
}

void Talos_loop() {
    /**
     * Looping code. Stopped when in error state.
     */
}

void Talos_onError() {
    /**
     * Looping code when in error state.
     */
}

void Core_priorityLoop() {
    /**
     * Looping code no matter what the current state is.
     */
}

void Hermes_onMessage(Hermes_t *hermesInstance, uint8_t sender,
                      uint16_t command, uint32_t isResponse) {
    /**
     * Code to be run every time a message is received over the CAN network.
     */
}